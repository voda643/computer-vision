import numpy as np
from skimage.measure import label
from scipy.ndimage import morphology

image = np.load('ps.npy.txt')

masks = np.array([
                  np.array([
                            [1, 1, 1, 1, 1, 1],
                            [1, 1, 1, 1, 1, 1],
                            [1, 1, 1, 1, 1, 1],
                            [1, 1, 1, 1, 1, 1]
                  ]),
                  np.array([
                            [1, 1, 0, 0, 1, 1],
                            [1, 1, 0, 0, 1, 1],
                            [1, 1, 1, 1, 1, 1],
                            [1, 1, 1, 1, 1, 1]
                  ]),
                  np.array([
                            [1, 1, 1, 1, 1, 1],
                            [1, 1, 1, 1, 1, 1],
                            [1, 1, 0, 0, 1, 1],
                            [1, 1, 0, 0, 1, 1]
                  ]),
                  np.array([
                            [1, 1, 1, 1],
                            [1, 1, 1, 1],
                            [1, 1, 0, 0],
                            [1, 1, 0, 0],
                            [1, 1, 1, 1],
                            [1, 1, 1, 1]
                  ]),
                  np.array([
                            [1, 1, 1, 1],
                            [1, 1, 1, 1],
                            [0, 0, 1, 1],
                            [0, 0, 1, 1],
                            [1, 1, 1, 1],
                            [1, 1, 1, 1]
                  ])
], dtype=object)


def count(image, mask):
  erosion = morphology.binary_erosion(image, mask)
  dilation = morphology.binary_dilation(erosion, mask)
  image -= dilation
  count = label(dilation).max()
  return count

total_amount = 0
for i, mask in enumerate(masks):
  amount = count(image, mask)
  total_amount += amount
  print(amount)

print(total_amount)
